﻿using UnityEngine;

//TODO : Slipt FirstPersonController script into smaller scripts
namespace TheWallsBleed
{
    using Camera;

    [RequireComponent(typeof(CharacterController))]
    [RequireComponent(typeof(AudioSource))]
    public class PlayerController : MonoBehaviour
    {
        [Header("Movement")]
        [SerializeField] private float _walkSpeed = 6;
        [SerializeField] private float _sprintSpeed = 12;
        [SerializeField] private float _sneakSpeed = 3;
        [SerializeField] private float _slideSpeed = 12.0f;

        [Header("Crouch")]
        [SerializeField] private float _couchHeightMultiplier = 0.5f;
        [SerializeField] private float _crouchingSpeed = 5f;

        [Header("Forces")]
        [SerializeField] private float _stickToGroundForce = 20;
        [SerializeField] private float _gravityMultiplier = 1;
        [SerializeField] private float _antiBumpFactor = .75f;

        [Header("Flags")]
        [SerializeField] private bool _slideWhenOverSlopeLimit = true;
        [SerializeField] private bool _airControl = false;
        [SerializeField] private bool _autoWalk = false;

        [Header("Camera")]
        [SerializeField] private MouseLook _mouseLook;
        public MouseLook MouseLook { get => _mouseLook; }

        [Header("Fall")]
        [SerializeField] private FallDamage _fallDamage;

        [Header("Audio")]
        [SerializeField] private float _stepInterval = 1.0f;
        [SerializeField] private AudioClip[] _footstepSounds;    // an array of footstep sounds that will be randomly selected from.
        [SerializeField] private AudioClip _landSound;           // the sound played when character touches back on ground.

        private CharacterController _characterController;
        private UnityEngine.Camera _camera;
        private AudioSource _audioSource;
        
        private bool _previouslyGrounded;
        private bool _playerControl = false;
        private float _speed;
        private float _startHeight;
        private float _newHeight;
        private float _yRotation;
        private float _stepCycle;
        private float _nextStep;
        private float _slideLimit;
        private float _rayDistance;
        private Vector2 _input;
        private Vector3 _moveDir = Vector3.zero;
        private Vector3 _contactPoint;
        private RaycastHit _hit;
        private CollisionFlags _collisionFlags;

        private void Start()
        {
            _characterController = GetComponent<CharacterController>();
            _camera = UnityEngine.Camera.main;
            _audioSource = GetComponent<AudioSource>();

            _speed = _walkSpeed;
            _startHeight = _characterController.height;
            _stepCycle = 0f;
            _nextStep = _stepCycle / 2f;
            _slideLimit = _characterController.slopeLimit - .1f;
            _rayDistance = _characterController.height * .5f + _characterController.radius;
            _mouseLook.Init(transform, _camera.transform);
            _fallDamage.Init(transform);
        }
        private void Update()
        {
            RotateView();

            if (_previouslyGrounded == false &&
                _characterController.isGrounded == true)
            {
                PlayLandingSound();
                _moveDir.y = 0f;
            }
            if (_previouslyGrounded == true &&
                _characterController.isGrounded == false)
            {
                _moveDir.y = 0f;
            }

            _previouslyGrounded = _characterController.isGrounded;
        }
        private void FixedUpdate()
        {
            _input = _input.sqrMagnitude > 1 ? _input.normalized : _input;
            
            if (_autoWalk)
                _input.y = 1;

            // always move along the camera forward as it is the direction that it being aimed at
            Vector3 desiredMove = transform.forward * _input.y + transform.right * _input.x;

            // get a normal for the surface that is being touched to move along it
            Physics.SphereCast(transform.position, _characterController.radius, Vector3.down, out RaycastHit hitInfo,
                               _characterController.height / 2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
            desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

            _moveDir.x = desiredMove.x * _speed;
            _moveDir.z = desiredMove.z * _speed;

            if (_characterController.isGrounded)
            {
                bool sliding = false;

                _fallDamage.EndFalling();

                // See if surface immediately below should be slid down. We use this normally rather than a ControllerColliderHit point,
                // because that interferes with step climbing amongst other annoyances
                if (Physics.Raycast(transform.position, -Vector3.up, out _hit, _rayDistance))
                {
                    if (Vector3.Angle(_hit.normal, Vector3.up) > _slideLimit)
                    {
                        sliding = true;
                    }
                }
                // However, just raycasting straight down from the center can fail when on steep slopes
                // So if the above raycast didn't catch anything, raycast down from the stored ControllerColliderHit point instead
                else
                {
                    Physics.Raycast(_contactPoint + Vector3.up, -Vector3.up, out _hit);
                    if (Vector3.Angle(_hit.normal, Vector3.up) > _slideLimit)
                    {
                        sliding = true;
                    }
                }

                // If sliding (and it's allowed), or if we're on an object tagged "Slide", get a vector pointing down the slope we're on
                if (sliding && _slideWhenOverSlopeLimit)
                {
                    Vector3 hitNormal = _hit.normal;
                    _moveDir = new Vector3(hitNormal.x, -hitNormal.y, hitNormal.z);
                    Vector3.OrthoNormalize(ref hitNormal, ref _moveDir);
                    _moveDir *= _slideSpeed;
                    _playerControl = false;
                }
                // Otherwise recalculate moveDirection directly from axes, adding a bit of -y to avoid bumping down inclines
                else
                {
                    _moveDir = new Vector3(_input.x, -_antiBumpFactor, _input.y);
                    _moveDir = transform.TransformDirection(_moveDir) * _speed;
                    _moveDir.y = -_stickToGroundForce;
                    _playerControl = true;
                }

            }
            else
            {
                _fallDamage.StartFalling();

                // If air control is allowed, check movement but don't touch the y component
                if (_airControl && _playerControl)
                {
                    _moveDir.x = _input.x * _speed;
                    _moveDir.z = _input.y * _speed;
                    _moveDir = transform.TransformDirection(_moveDir);
                }
            }

            _moveDir += Physics.gravity * _gravityMultiplier * Time.fixedDeltaTime;

            _collisionFlags = _characterController.Move(_moveDir * Time.fixedDeltaTime);

            float lastHeight = _characterController.height;

            _characterController.height = Mathf.Lerp(_characterController.height, _newHeight, _crouchingSpeed * Time.fixedDeltaTime);
            transform.position += Vector3.up * ((_characterController.height - lastHeight) * _couchHeightMultiplier);

            ProgressStepCycle(_speed);
            _mouseLook.UpdateCursorLock();
        }
        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            _contactPoint = hit.point;

            Rigidbody body = hit.collider.attachedRigidbody;
            //dont move the rigidbody if the character is on top of it
            if (_collisionFlags == CollisionFlags.Below)
            {
                return;
            }

            if (body == null || body.isKinematic)
            {
                return;
            }
            body.AddForceAtPosition(_characterController.velocity * 0.1f, hit.point, ForceMode.Impulse);
        }

        public void SetMoveX(float horizontal)
        {
            _input.x = horizontal;
        }
        public void SetMoveY(float vertical)
        {
            _input.y = vertical;
        }
        public void SetSneaking(bool toggle)
        {
            _newHeight = _startHeight;

            if (toggle)
            {
                _speed = _sneakSpeed;
                _newHeight = _couchHeightMultiplier * _startHeight;
            }
            else
            {
                _speed = _walkSpeed;
            }
        }
        public void SetSprinting(bool toggle)
        {
            if (toggle)
            {
                _speed = _sprintSpeed;
            }
            else
            {
                _speed = _walkSpeed;
            }
        }

        private void RotateView()
        {
            _mouseLook.LookRotation(transform, _camera.transform);
        }

        private void ProgressStepCycle(float speed)
        {
            if (_characterController.velocity.sqrMagnitude > 0 && (_input.x != 0 || _input.y != 0))
            {
                _stepCycle += (_characterController.velocity.magnitude + speed) * Time.fixedDeltaTime;
            }

            if (!(_stepCycle > _nextStep))
            {
                return;
            }

            _nextStep = _stepCycle + _stepInterval;

            PlayFootStepAudio();
        }
        private void PlayFootStepAudio()
        {
            if (!_characterController.isGrounded)
            {
                return;
            }

            if (_footstepSounds == null || _footstepSounds.Length <= 0)
            {
                return;
            }
            // pick & play a random footstep sound from the array,
            // excluding sound at index 0
            int n = Random.Range(1, _footstepSounds.Length);
            _audioSource.clip = _footstepSounds[n];
            _audioSource.PlayOneShot(_audioSource.clip);
            // move picked sound to index 0 so it's not picked next time
            _footstepSounds[n] = _footstepSounds[0];
            _footstepSounds[0] = _audioSource.clip;
        }
        private void PlayLandingSound()
        {
            if (_landSound == null)
            {
                return;
            }

            _audioSource.clip = _landSound;
            _audioSource.Play();
            _nextStep = _stepCycle + .5f;
        }
    }

    //[RequireComponent(typeof(CharacterController))]
    //public sealed class PlayerController_0 : EntityController<PlayerController_0>
    //{
    //    [SerializeField]
    //    private PlayerInput _playerInput;
    //    public PlayerInput PlayerInput { get => _playerInput; }

    //    [SerializeField]
    //    private PlayerRaycast _playerRaycast;
    //    public PlayerRaycast PlayerRaycast { get => _playerRaycast; }

    //    [SerializeField]
    //    private MouseLook _mouseLook;
    //    public MouseLook MouseLook { get => _mouseLook; }

    //    [Serializable]
    //    private struct PlayerMovement
    //    {
    //        public float walkSpeed;
    //        public float sprintSpeed;
    //        public float sneakSpeed;
    //        public float slideSpeed;
    //    }
    //    [SerializeField]
    //    private PlayerMovement _playerMovement;

    //    private Movement _movement;

    //    private CharacterController _characterController;
    //    private UnityEngine.Camera _camera;

    //    private void Start()
    //    {
    //        _characterController = GetComponent<CharacterController>();
    //        _camera = UnityEngine.Camera.main;

    //        _movement = new Movement(_playerMovement.walkSpeed);
    //    }

    //    protected override void Update()
    //    {
    //        base.Update();
    //    }
    //    private void FixedUpdate()
    //    {

    //    }
    //}

    //[System.Serializable]
    //public class Movement
    //{
    //    private float _currentSpeed;

    //    public Movement(float startSpeed)
    //    {
    //        _currentSpeed = startSpeed;
    //    }


    //    public void UpdateMovement()
    //    {

    //    }
    //}
}