﻿using System;
using UnityEngine;

namespace TheWallsBleed
{
    [Serializable]
    public class FallDamage
    {
        [SerializeField] private bool _enableFallingDamage = true;
        [SerializeField] private float _fallingDamageThreshold = 10.0f;
        [SerializeField] private float _fallingDamageMultipler = 1;

        private Transform _character;
        private Health _health;
        private bool falling;
        private float fallStartLevel;

        public void Init(Transform character)
        {
            _character = character;
            _health = _character.GetComponent<Health>();
        }

        public void EndFalling()
        {
            // If we were falling, and we fell a vertical distance greater than the threshold, run a falling damage routine
            if (falling == true)
            {
                falling = false;
                if (_character.position.y < fallStartLevel - _fallingDamageThreshold &&
                    _enableFallingDamage == true)
                {
                    ApplyFallingDamage(fallStartLevel - _character.position.y);
                }
            }
        }

        public void StartFalling()
        {
            // If we stepped over a cliff or something, set the height at which we started falling
            if (falling == false)
            {
                falling = true;
                fallStartLevel = _character.position.y;
            }
        }

        // If falling damage occured, this is the place to do something about it. You can make the player
        // have hitpoints and remove some of them based on the distance fallen, add sound effects, etc.
        private void ApplyFallingDamage(float fallDistance)
        {
            _health.ApplyDamage(fallDistance * _fallingDamageMultipler);
        }
    }
}