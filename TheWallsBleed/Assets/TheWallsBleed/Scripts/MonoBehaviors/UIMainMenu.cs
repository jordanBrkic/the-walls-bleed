﻿using UnityEngine;
using UnityEngine.UI;

using UnityEngine.SceneManagement;

namespace TheWallsBleed.UI
{
    using Managers;

    public class UIMainMenu : MonoBehaviour
    {
        [Header("Buttons")]
        [SerializeField]
        private Button _newGame;

        [SerializeField]
        private Button _loadGame;

        [SerializeField]
        private Button _options;

        [SerializeField]
        private Button _keyBindings;

        [SerializeField]
        private Button _quit;

        [Header("Menus")]
        [SerializeField]
        private Canvas _mainMenu;

        [SerializeField]
        private Canvas _optionsMenu;

        [SerializeField]
        private Canvas _keyBindingsMenu;

        [Header("Scenes")]
        [SerializeField]
        private string _newGameScene;

        [SerializeField]
        private string _loadGameScene;

        //----
        private void OnEnable()
        {
            _newGame.onClick.AddListener(NewGame);
            _loadGame.onClick.AddListener(LoadGame);
            //_options.onClick.AddListener(() => GoToMenu(_optionsMenu));
            _keyBindings.onClick.AddListener(() => GoToMenu(_keyBindingsMenu));
            _quit.onClick.AddListener(QuitGame);
        }
        private void Start()
        {
            _mainMenu.gameObject.SetActive(true);
        }
        private void OnDisable()
        {
            _newGame.onClick.RemoveAllListeners();
            _loadGame.onClick.RemoveAllListeners();
            //_options.onClick.RemoveAllListeners();
            _keyBindings.onClick.RemoveAllListeners();
            _quit.onClick.RemoveAllListeners();
        }

        //----
        private void NewGame()
        {
            SceneManager.Instance.SwitchScene(_newGameScene);
        }
        private void LoadGame()
        {
            SceneManager.Instance.SwitchScene(_loadGameScene);
        }
        private void GoToMenu(Canvas menu)
        {
            menu.gameObject.SetActive(true);
            _mainMenu.gameObject.SetActive(false);
        }
        private void QuitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
    }
}

namespace TheWallsBleed.Managers
{
    using SM = UnityEngine.SceneManagement.SceneManager;

    public class SceneManager : Manager<SceneManager>
    {
        public void SwitchScene(string sceneName)
        {
            SM.LoadScene(sceneName, LoadSceneMode.Single);
        }
    }
}