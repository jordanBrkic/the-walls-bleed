﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TheWallsBleed
{
    [RequireComponent(typeof(MeshFilter))]
    public class FieldOfView : MonoBehaviour
    {
        [SerializeField]
        private bool _drawFOV;

        [SerializeField]
        private float _viewRadius = 0.0f;
        public float ViewRadius { get => _viewRadius; }

        [SerializeField]
        [Range(0, 360)]
        private float _viewAngle = 0.0f;
        public float ViewAngle { get => _viewAngle; }

        [SerializeField]
        private LayerMask _targetMask = 0;

        [SerializeField]
        private LayerMask _obstacleMask = 0;

        private List<Transform> _visialbeTargets = new List<Transform>();
        public List<Transform> VisialbeTargets { get => _visialbeTargets; }

        [SerializeField]
        private float _meshResoultion = 0.0f;

        [SerializeField]
        private int _edgeResolveIterations = 0;

        [SerializeField]
        private int _edgeDstThreshold = 0;

        [SerializeField]
        private MeshFilter _viewMeshFilter;
        private Mesh _viewMesh;

        private Job _job;

        private struct ViewCastInfo
        {
            public bool hit;
            public Vector3 point;
            public float dist;
            public float angle;

            public ViewCastInfo(bool hit, Vector3 point, float dist, float angle)
            {
                this.hit = hit;
                this.point = point;
                this.dist = dist;
                this.angle = angle;
            }
        }

        private struct EdgeInfo
        {
            public Vector3 pointA;
            public Vector3 pointB;

            public EdgeInfo(Vector3 pointA, Vector3 pointB)
            {
                this.pointA = pointA;
                this.pointB = pointB;
            }
        }

        private void Awake()
        {
            _job = Job.Make(FindTargetWithDelay(0.2f), true);

            if (_viewMeshFilter != null)
            {
                _viewMesh = new Mesh() { name = "ViewMesh" };
                _viewMeshFilter.mesh = _viewMesh;
            }
        }

        private void OnEnable()
        {
            _job.UnPause();
        }

#if UNITY_EDITOR
        private void LateUpdate()
        {
            if (_drawFOV)
                DrawFieldOfView();
        }
#endif

        private void OnDisable()
        {
            _job.Pause();
        }

        public Vector3 DirFromAngle(float angleInDegress, bool angleIsGlobal)
        {
            if (!angleIsGlobal)
                angleInDegress += transform.eulerAngles.y;

            return new Vector3(Mathf.Sin(angleInDegress * Mathf.Deg2Rad), 0.0f, Mathf.Cos(angleInDegress * Mathf.Deg2Rad));
        }

        private void FindVisibleTargets()
        {
            _visialbeTargets.Clear();

            Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, _viewRadius, _targetMask);

            for (int i = 0; i < targetsInViewRadius.Length; i++)
            {
                Transform target = targetsInViewRadius[i].transform;
                Vector3 dirToTarget = (target.transform.position - transform.position).normalized;
                if (Vector3.Angle(transform.forward, dirToTarget) < ViewAngle / 2)
                {
                    float disToTarget = Vector3.Distance(transform.position, target.position);

                    if (!Physics.Raycast(transform.position, dirToTarget, disToTarget, _obstacleMask))
                    {
                        _visialbeTargets.Add(target);
                    }
                }
            }
        }

        private void DrawFieldOfView()
        {
            int stepCount = Mathf.RoundToInt(_viewAngle * _meshResoultion);
            float stepAngleSize = _viewAngle / stepCount;

            List<Vector3> viewPoints = new List<Vector3>();

            ViewCastInfo oldViewCast = new ViewCastInfo();

            for (int i = 0; i < stepCount; i++)
            {
                float angle = transform.eulerAngles.y - _viewAngle / 2 + stepAngleSize * i;
                ViewCastInfo newViewCast = ViewCast(angle);

                bool edgeDstThresholdExceeded = Mathf.Abs(oldViewCast.dist - newViewCast.dist) > _edgeDstThreshold;

                if (i > 0)
                {
                    if (oldViewCast.hit != newViewCast.hit || (oldViewCast.hit && newViewCast.hit && edgeDstThresholdExceeded))
                    {
                        EdgeInfo edge = FindEdge(oldViewCast, newViewCast);
                        if (edge.pointA != Vector3.zero)
                            viewPoints.Add(edge.pointA);
                        if (edge.pointB != Vector3.zero)
                            viewPoints.Add(edge.pointB);
                    }
                }

                viewPoints.Add(newViewCast.point);
                oldViewCast = newViewCast;
            }

            int vertexCount = viewPoints.Count + 1;
            Vector3[] vertices = new Vector3[vertexCount];
            int[] triangles = new int[(vertexCount - 2) * 3];

            vertices[0] = Vector3.zero;

            for (int i = 0; i < vertexCount - 1; i++)
            {
                vertices[i + 1] = transform.InverseTransformPoint(viewPoints[i]);

                if (i < vertexCount - 2)
                {
                    triangles[i * 3] = 0;
                    triangles[i * 3 + 1] = i + 1;
                    triangles[i * 3 + 2] = i + 2;
                }
            }

            _viewMesh.Clear();
            _viewMesh.vertices = vertices;
            _viewMesh.triangles = triangles;
            _viewMesh.RecalculateNormals();
        }

        private ViewCastInfo ViewCast(float gloabalAngle)
        {
            Vector3 dir = DirFromAngle(gloabalAngle, true);
            if (Physics.Raycast(transform.position, dir, out RaycastHit hit, _viewRadius, _obstacleMask))
                return new ViewCastInfo(true, hit.point, hit.distance, gloabalAngle);
            else
                return new ViewCastInfo(false, transform.position + dir * _viewRadius, _viewRadius, gloabalAngle);
        }

        private EdgeInfo FindEdge(ViewCastInfo minViewCast, ViewCastInfo maxViewCast)
        {
            float minAngle = minViewCast.angle;
            float maxAngle = maxViewCast.angle;
            Vector3 minPoint = Vector3.zero;
            Vector3 maxPoint = Vector3.zero;

            for (int i = 0; i < _edgeResolveIterations; i++)
            {
                float angle = (minAngle + maxAngle) / 2;
                ViewCastInfo newViewCast = ViewCast(angle);

                bool edgeDstThresholdExceeded = Mathf.Abs(minViewCast.dist - newViewCast.dist) > _edgeDstThreshold;

                if (newViewCast.hit == minViewCast.hit && !edgeDstThresholdExceeded)
                {
                    minAngle = angle;
                    minPoint = newViewCast.point;
                }
                else
                {
                    maxAngle = angle;
                    maxPoint = newViewCast.point;
                }
            }

            return new EdgeInfo(minPoint, maxPoint);

        }

        private IEnumerator FindTargetWithDelay(float delay)
        {
            while (true)
            {
                yield return new WaitForSeconds(delay);
                FindVisibleTargets();
            }
        }
    }
}