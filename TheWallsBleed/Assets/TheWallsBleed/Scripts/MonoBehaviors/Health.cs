﻿using UnityEngine;

namespace TheWallsBleed
{
    public class Health : MonoBehaviour
    {
        [SerializeField] private float _maxHealth = 100;
        [SerializeField] private bool _regenerate = false;
        [SerializeField] private float _regenerateSpeed = 5;
        [SerializeField] private float _delayToRegenerate = 5;

        private float _curHealth;
        public float GetMaxHealth { get { return _maxHealth; } }

        private float _nextRegnerationTime = 0;
        public float GetCurrentHealth { get { return _curHealth; } }

        public delegate void Death();
        public event Death OnDeath;

        private void Awake()
        {
            _curHealth = _maxHealth;
        }
        private void Update()
        {
            _curHealth = Mathf.Clamp(_curHealth, 0, _maxHealth);

            RegenerateHealth();
        }

        private void RegenerateHealth()
        {
            if (_regenerate == true &&
                _curHealth < _maxHealth &&
                _curHealth > 0 &&
                _nextRegnerationTime < Time.time)
            {
                _curHealth += _regenerateSpeed * Time.deltaTime;
            }
        }


        public void ApplyDamage(float _value)
        {
            _curHealth -= _value;

            _nextRegnerationTime = _delayToRegenerate + Time.time;

            if (_curHealth <= 0)
                OnDeath?.Invoke();
        }
    }
}