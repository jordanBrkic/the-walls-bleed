﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.Collections;

namespace TheWallsBleed
{
    public interface IState<T>
    {
        void Exit(T owner);
        void Reason(T owner);
        void Enter(T owner);
        void Execute(T owner);
    }
}