﻿using System;
using UnityEngine;

namespace TheWallsBleed.Camera
{
    using Input = UnityEngine.Input;

    [Serializable]
    public class MouseLook
    {
        public float XSensitivity = 2f;
        public float YSensitivity = 2f;
        public bool ClampVerticalRotation = true;
        public float MinimumX = -90F;
        public float MaximumX = 90F;
        public bool Smooth;
        public float SmoothTime = 5f;
        public bool LockCursor = true;

        private float _mouseX;
        private float _mouseY;
        private Quaternion _characterTargetRotation;
        private Quaternion _cameraTargetRotation;
        private bool _cursorIsLocked = true;

        public void Init(Transform character, Transform camera)
        {
            _characterTargetRotation = character.localRotation;
            _cameraTargetRotation = camera.localRotation;
        }
        
        public void SetMouseX(float mouseX)
        {
            _mouseX = mouseX;
        }
        public void SetMouseY(float mouseY)
        {
            _mouseY = mouseY;
        }

        public void LookRotation(Transform character, Transform camera)
        {
            float yRotation = _mouseX * XSensitivity;
            float xRotation = _mouseY * YSensitivity;

            _characterTargetRotation *= Quaternion.Euler(0f, yRotation, 0f);
            _cameraTargetRotation *= Quaternion.Euler(-xRotation, 0f, 0f);

            if (ClampVerticalRotation == true)
            {
                _cameraTargetRotation = ClampRotationAroundXAxis(_cameraTargetRotation);
            }

            if (Smooth == true)
            {
                character.localRotation = Quaternion.Slerp(character.localRotation, _characterTargetRotation, SmoothTime * Time.deltaTime);
                camera.localRotation = Quaternion.Slerp(camera.localRotation, _cameraTargetRotation, SmoothTime * Time.deltaTime);
            }
            else
            {
                character.localRotation = _characterTargetRotation;
                camera.localRotation = _cameraTargetRotation;
            }

            UpdateCursorLock();
        }

        public void SetCursorLock(bool value)
        {
            LockCursor = value;
            if (LockCursor == false)
            {
                //we force unlock the cursor if the user disable the cursor locking helper
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }
        public void UpdateCursorLock()
        {
            //if the user set "lockCursor" we check & properly lock the cursos
            if (LockCursor)
            {
                InternalLockUpdate();
            }
        }
        private void InternalLockUpdate()
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                _cursorIsLocked = false;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                _cursorIsLocked = true;
            }

            if (_cursorIsLocked == true)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }

        private Quaternion ClampRotationAroundXAxis(Quaternion q)
        {
            q.x /= q.w;
            q.y /= q.w;
            q.z /= q.w;
            q.w = 1.0f;

            float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

            angleX = Mathf.Clamp(angleX, MinimumX, MaximumX);

            q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

            return q;
        }
    }
}