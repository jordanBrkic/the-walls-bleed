﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace TheWallsBleed
{    
    public class SoundManager : Manager<SoundManager>
    {
        private Dictionary<string, AudioSource> _audioSources;

        public void Add(string name, AudioSource audioSource)
        {
            if (_audioSources == null)
                _audioSources = new Dictionary<string, AudioSource>();

            if (!_audioSources.ContainsKey(name))
                _audioSources.Add(name, audioSource);
        }
        public void Remove(string name)
        {
            if (_audioSources == null || _audioSources.Count == 0)
                return;

            if (_audioSources.ContainsKey(name))
                _audioSources.Remove(name);
        }
        public void Clear()
        {
            if (_audioSources == null || _audioSources.Count == 0)
                return;

            _audioSources.Clear();
        }

        public void Play(string name)
        {
            _audioSources[name].Play();
        }
        public void Pause(string name)
        {
            _audioSources[name].Pause();
        }
        public void UnPause(string name)
        {
            _audioSources[name].UnPause();
        }
        public void Stop(string name)
        {
            _audioSources[name].Stop();
        }

        public void PauseAll()
        {
            foreach (var audioSource in _audioSources.Values)
                audioSource.Pause();
        }
        public void UnPauseAll()
        {
            foreach (var audioSource in _audioSources.Values)
                audioSource.UnPause();
        }
        public void StopAll()
        {
            foreach (var audioSource in _audioSources.Values)
                audioSource.Stop();
        }
    }
}
