﻿namespace TheWallsBleed
{
    public class StateMachine<T>
    {
        private IState<T> _currrentState;
        private IState<T> _previousState;

        private T _owner;

        public StateMachine(T owner)
        {
            _owner = owner;
            _currrentState = null;
            _previousState = null;
        }

        public void ChangeState(IState<T> newState)
        {
            if (_currrentState != null)
            _currrentState.Exit(_owner);

            _previousState = _currrentState;
            _currrentState = newState;

            _currrentState.Enter(_owner);
        }

        public void RevertToPreviousState()
        {
            ChangeState(_previousState);
        }

        public void Update()
        {
            if (_currrentState != null)
            {
                _currrentState.Reason(_owner);
                _currrentState.Execute(_owner);
            }
        }
    }
}