﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace TheWallsBleed
{
    public class Job
    {
        public event System.Action<bool> JobComplete;

        private bool _running;
        public bool GetRunning { get => _running; }

        private bool _pause;
        public bool GetPause { get => _pause; }

        private IEnumerator _coroutine;
        private bool _jobWaskilled;
        private Stack<Job> _childJobStack;

        private Job(IEnumerator coroutine, bool shouldStart = false)
        {
            _coroutine = coroutine;
            if (shouldStart)
                Start();
        }

        public static Job Make(IEnumerator coroutine, bool shouldStart = false)
        {
            return new Job(coroutine, shouldStart);
        }

        public Job CreateAndAddChildJob(IEnumerator coroutine)
        {
            var j = new Job(coroutine, false);
            AddChildJob(j);
            return j;
        }

        public void AddChildJob(Job childJob)
        {
            if (_childJobStack == null)
                _childJobStack = new Stack<Job>();

            _childJobStack.Push(childJob);
        }

        public void RemoveChildJob(Job childJob)
        {
            if (_childJobStack == null)
                return;

            if (_childJobStack.Contains(childJob))
            {
                var childStack = new Stack<Job>(_childJobStack.Count - 1);
                var allCurrentChildren = _childJobStack.ToArray();
                System.Array.Reverse(allCurrentChildren);

                for (int i = 0; i < allCurrentChildren.Length; i++)
                {
                    var j = allCurrentChildren[i];
                    if (j != childJob)
                        childStack.Push(j);
                }

                _childJobStack = childStack;
            }
        }

        public void Start()
        {
            _running = true;
            Manager.Instance.StartCoroutine(DoWork());
        }

        public IEnumerator StartAsCoroutine()
        {
            _running = true;
            yield return Manager.Instance.StartCoroutine(DoWork());
        }

        public void Pause()
        {
            _pause = true;
        }

        public void UnPause()
        {
            _pause = false;
        }

        public void Kill()
        {
            _jobWaskilled = true;
            _running = false;
            _pause = false;
        }

        public void Kill(float delayInSeconds)
        {
            var delay = (int)(delayInSeconds * 1000);
            new System.Threading.Timer(obj =>
            {
                lock (this)
                {
                    Kill();
                }
            }, null, delay, System.Threading.Timeout.Infinite);
        }

        private IEnumerator DoWork()
        {
            yield return null;

            while (_running)
            {
                if (_pause)
                    yield return null;
                else
                {
                    if (_coroutine.MoveNext())
                        yield return _coroutine.Current;
                    else
                    {
                        if (_childJobStack != null)
                            yield return Manager.Instance.StartCoroutine(RunChildJobs());
                        _running = false;
                    }
                }
            }
            JobComplete?.Invoke(_jobWaskilled);
        }

        private IEnumerator RunChildJobs()
        {
            if (_childJobStack != null && _childJobStack.Count > 0)
            {
                do
                {
                    Job childJob = _childJobStack.Pop();
                    yield return Manager.Instance.StartCoroutine(childJob.StartAsCoroutine());
                }
                while (_childJobStack.Count > 0);
            }
        }
    }
}