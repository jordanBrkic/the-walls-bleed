﻿using UnityEngine;
using System.Collections.Generic;

namespace TheWallsBleed
{
    using Input = UnityEngine.Input;

    public enum ButtonState
    {
        DOWN,
        UP,
        HELD
    }

    public class InputManager : Manager<InputManager>
    {
        public delegate void InputAxis(float value);
        public delegate void InputButton(ButtonState buttonState);

        private static Dictionary<string, List<InputAxis>> _inputAxisDic;
        private static Dictionary<string, List<InputButton>> _inputButtonDic;

        public void AddAxis(string key, InputAxis callback)
        {
            if (_inputAxisDic == null)
                _inputAxisDic = new Dictionary<string, List<InputAxis>>();

            if (_inputAxisDic.ContainsKey(key) == false)
                _inputAxisDic.Add(key, new List<InputAxis>());
            
            if (_inputAxisDic[key].Contains(callback) == false)
                _inputAxisDic[key].Add(callback);
        }
        public void RemoveAxis(string key, InputAxis callback)
        {
            if (_inputAxisDic == null)
                return;

            if (_inputAxisDic.ContainsKey(key))
            {
                if (_inputAxisDic[key].Contains(callback))
                    _inputAxisDic[key].Remove(callback);
            }
        }

        public void AddButton(string key, InputButton callback)
        {
            if (_inputButtonDic == null)
                _inputButtonDic = new Dictionary<string, List<InputButton>>();

            if (_inputButtonDic.ContainsKey(key) == false)
                _inputButtonDic.Add(key, new List<InputButton>());

            if (_inputButtonDic[key].Contains(callback) == false)
                _inputButtonDic[key].Add(callback);
        }
        public void RemoveButton(string key, InputButton callback)
        {
            if (_inputButtonDic == null)
                return;

            if (_inputButtonDic.ContainsKey(key))
            {
                if (_inputButtonDic[key].Contains(callback))
                    _inputButtonDic[key].Remove(callback);
            }
        }

        private void Update()
        {
            UpdateAxis();
            UpdateButtons();
        }

        private void UpdateAxis()
        {
            if (_inputAxisDic == null)
                return;

            foreach (var kvp in _inputAxisDic)
            {
                if (kvp.Value == null)
                    return;

                foreach (var element in kvp.Value)
                    element?.Invoke(Input.GetAxis(kvp.Key));
            }
        }
        private void UpdateButtons()
        {
            if (_inputButtonDic == null)
                return;

            foreach (var kvp in _inputButtonDic)
            {
                if (kvp.Value == null)
                    return;

                foreach (var element in kvp.Value)
                {
                    if (Input.GetButtonUp(kvp.Key))
                        element?.Invoke(ButtonState.UP);
                    else if (Input.GetButtonDown(kvp.Key))
                        element?.Invoke(ButtonState.DOWN);
                    else if (Input.GetButton(kvp.Key))
                        element?.Invoke(ButtonState.HELD);
                }
            }
        }
    }
}
