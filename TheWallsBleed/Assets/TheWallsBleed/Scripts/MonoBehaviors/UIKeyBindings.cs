﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace TheWallsBleed.UI
{
    //[ExecuteInEditMode]
    public class UIKeyBindings : MonoBehaviour
    {
        [SerializeField]
        private Canvas _keyBindingsMenu;

        [System.Serializable]
        private struct UIBindingSettings
        {
            public GameObject prefab;
            public Transform parent;
            public int spacing;
        }

        [SerializeField]
        private UIBindingSettings _uiSettings;
        
        [SerializeField]
        private KeyBinding[] _bindings;

        private class KeyBindingUIObject
        {
            public Image background;
            public Text label;
            public InputField primaryKey;
            public InputField secondaryKey;
            
            public KeyBindingUIObject(GameObject prefab, KeyBinding keyBinding)
            {
                background = prefab.GetComponentInChildren<Image>();
                label = prefab.GetComponentInChildren<Text>();

                var inputFeilds = prefab.GetComponentsInChildren<InputField>();
                primaryKey = inputFeilds[0];
                secondaryKey = inputFeilds[1];

                label.text = keyBinding.keyName;
                primaryKey.text = keyBinding.primaryKey;
                secondaryKey.text = keyBinding.secondaryKey;
            }
        }

        private Dictionary<KeyBinding, KeyBindingUIObject> _bindingToUIMap;

        private void Start()
        {
            CreateUIObjects();
        }

        public void CreateUIObjects()
        {
            if (_uiSettings.prefab == null)
                return;

            for (int i = 0; i < _bindings.Length; i++)
            {
                var prefabCopy = Instantiate(_uiSettings.prefab, _uiSettings.parent);
                var uiObject = new KeyBindingUIObject(prefabCopy, _bindings[i]);
            }
        }

        private void DestroyUIBindingObject()
        {

        }
    }

}

namespace TheWallsBleed
{
    [System.Serializable]
    public struct KeyBinding
    {
        public string keyName;
        public string primaryKey;
        public string secondaryKey;

        public KeyBinding(string keyName, string primaryKey, string secondaryKey)
        {
            this.keyName = keyName;
            this.primaryKey = primaryKey;
            this.secondaryKey = secondaryKey;
        }
    }
}
