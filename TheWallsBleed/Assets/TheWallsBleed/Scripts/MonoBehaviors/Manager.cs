﻿using UnityEngine;

namespace TheWallsBleed
{
    public abstract class Manager<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static bool _applicationIsQuitting = false;
        private static object _lock = new object();

        private static T _instance;
        public static T Instance
        {
            get
            {
                if (_applicationIsQuitting)
                    return null;

                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = (T)FindObjectOfType(typeof(T));

                        if (FindObjectsOfType(typeof(T)).Length > 1)
                        {
                            Debug.LogError("[Singleton] Something went really wrong " +
                                " - there should never be more than 1 singleton!" +
                                " Reopening the scene might fix it.");
                            return _instance;
                        }


                        if (_instance == null)
                        {
                            GameObject singleton = new GameObject();
                            _instance = singleton.AddComponent<T>();
                            singleton.name = "(singleton) " + typeof(T).ToString();

                            DontDestroyOnLoad(singleton);

                            GameObject manager = GameObject.Find("Manager");
                            if (manager == null)
                                manager = new GameObject("Manager");
                            singleton.transform.SetParent(manager.transform);
                        }
                    }
                    return _instance;
                }
            }
        }

        protected virtual void OnApplicationQuit()
        {
            _instance = null;
        }
    }
    public class Manager : Manager<Manager>
    { }
}