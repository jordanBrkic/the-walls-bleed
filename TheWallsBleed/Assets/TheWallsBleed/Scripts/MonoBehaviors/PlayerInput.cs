﻿using UnityEngine;

namespace TheWallsBleed
{ 
    public class PlayerInput : MonoBehaviour
    {
        [SerializeField]
        private PlayerController _playerController;

        private void OnEnable()
        {
            InputManager.Instance.AddAxis("Horizontal", MoveX);
            InputManager.Instance.AddAxis("Vertical", MoveY);
            InputManager.Instance.AddAxis("Mouse X", MouseX);
            InputManager.Instance.AddAxis("Mouse Y", MouseY);
            InputManager.Instance.AddButton("Sprint", SetSprinting);
            InputManager.Instance.AddButton("Sneak", SetSneaking);
        }
        private void OnDisable()
        {
            InputManager.Instance.RemoveAxis("Horizontal", MoveX);
            InputManager.Instance.RemoveAxis("Vertical", MoveY);
            InputManager.Instance.RemoveAxis("Mouse X", MouseX);
            InputManager.Instance.RemoveAxis("Mouse Y", MouseY);
            InputManager.Instance.RemoveButton("Sprint", SetSprinting);
            InputManager.Instance.RemoveButton("Sneak", SetSneaking);
        }

        private void MoveX(float value)
        {
            _playerController?.SetMoveX(value);
        }
        private void MoveY(float value)
        {
            _playerController?.SetMoveY(value);
        }

        private void SetSprinting(ButtonState buttonState)
        {
            _playerController?.SetSprinting(buttonState == ButtonState.HELD);
        }
        private void SetSneaking(ButtonState buttonState)
        {
            _playerController?.SetSneaking(buttonState == ButtonState.HELD);
        }

        private void MouseX(float value)
        {
            _playerController?.MouseLook?.SetMouseX(value);
        }
        private void MouseY(float value)
        {
            _playerController?.MouseLook?.SetMouseY(value);
        }
    }
}