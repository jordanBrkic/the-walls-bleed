﻿using System;
using UnityEngine;

namespace TheWallsBleed
{
    public class PlayerRaycast : MonoBehaviour
    {
        [SerializeField]
        private int _rayLength = 3;
        [SerializeField]
        private LayerMask _interactLayerMask;
        
        private void Update()
        {
            RaycastHit hit;
            Vector3 forward = this.transform.TransformDirection(Vector3.forward);

            if (Physics.Raycast(this.transform.position, forward, out hit, _rayLength, _interactLayerMask.value))
            {
                if (hit.collider.CompareTag("Interactable"))
                {
                }
            }
        }
    }
}
